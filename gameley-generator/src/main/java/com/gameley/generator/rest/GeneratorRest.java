package com.gameley.generator.rest;

import com.alibaba.fastjson.JSON;

import com.gameley.common.msg.TableResultResponse;
import com.gameley.generator.service.GeneratorService;
import com.gameley.generator.utils.GeneratorModelUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.nio.cs.Surrogate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * Created by ace on 2017/8/25.
 */
@Controller
@RequestMapping("/base/generator")
public class GeneratorRest {

    @Autowired
    private GeneratorService generatorService;

    /**
     * 列表
     */
    @ResponseBody
    @RequestMapping("/page")
    public TableResultResponse<Map<String, Object>> list(@RequestParam Map<String, Object> params) {
        List<Map<String, Object>> result = generatorService.queryList(params);
        int total = generatorService.queryTotal(params);
        return new TableResultResponse<Map<String, Object>>(total, result);
    }

    /**
     * 生成代码
     */
    @RequestMapping("/code")
    public void code(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] tableNames = new String[]{};
        String tables = request.getParameter("tables");
        tableNames = JSON.parseArray(tables).toArray(tableNames);
        String menuid=request.getParameter("menuid");
        String parentid=request.getParameter("parentid");
        String path=request.getParameter("path");
        byte[] data = generatorService.generatorCode(tableNames,menuid,parentid,path);

        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"ag-admin-code.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");

        IOUtils.write(data, response.getOutputStream());
    }

    @RequestMapping(value="model")
    public void model(HttpServletRequest request,HttpServletResponse response){
        String modelname=request.getParameter("modelname");
        String port=request.getParameter("port");
        GeneratorModelUtils.generatorCode(modelname,port);
        try {
            // 以流的形式下载文件。
            File file = new File(GeneratorModelUtils.getBaseFile());
            InputStream fis = new BufferedInputStream(new FileInputStream(System.getProperty("user.dir")+File.separator+modelname+".zip"));
            //下载机器码文件
            response.setHeader("conent-type", "application/octet-stream");
            response.setContentType("application/octet-stream; charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename=" + modelname+".zip");

            OutputStream os = response.getOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(os);


            int length = 0;
            byte[] temp = new byte[1 * 1024 * 10];

            while ((length = fis.read(temp)) != -1) {
                bos.write(temp, 0, length);
            }
            bos.flush();
            fis.close();
            bos.close();
            file.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }







    }
}
